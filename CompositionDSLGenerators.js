const getDSLValue = (iterator, last) => {
  const {value, done} = iterator.next(last);
  if (done) {
    return value.slice(1).reduce((x, f) => f(x), value[0]);
  }
  return getDSLValue(iterator, last ? [...last, value] : [value]);
}
const pipe = gen => getDSLValue(gen(null, ));



const square = x => x**2;
const add = x => y => x + y;
const divideBy2 = x => x / 2;

const value = pipe(function*() {
  yield 5;
  yield square;
  yield add(5);
  const answer = yield divideBy2;

  return answer;
});

console.log(value);
// -> 15
